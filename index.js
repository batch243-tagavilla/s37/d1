const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Allows to access routes within our application
const userRoutes = require("./routes/userRoutes.js");

const app = express();
const port = 3000;

// allows all resources to access our backend application
app.use(cors());

// is a builtin middleware function in express. It parses incoming request with JSON payload
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Defines "/users" to be included for all user routes defined in the userRoutes route file
app.use("/users", userRoutes);

// DB connection
mongoose.connect(
    "mongodb+srv://admin:admin@zuittbatch243.fvxpkh7.mongodb.net/bookingAPI?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
);

mongoose.connection.on(
    "error",
    console.error.bind(console, "Connection Error")
);
mongoose.connection.once("open", () =>
    console.log("Successfully connected to MongoDB Atlas")
);

// process.env.PORT is that can be assigned by your hosting services
app.listen(process.env.PORT || port, () => {
    console.log(`API is now online on port ${process.env.PORT || port}`);
});
